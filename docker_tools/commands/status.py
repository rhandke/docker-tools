'''Status Command Module'''
import logging
import docker
import click
from rich.console import Console
from rich.table import Table
import fontawesome as fa

logger = logging.getLogger(__name__)


@click.command()
@click.option('--up', is_flag=True, help='Show only running containers')
def status(up):  # pylint: disable=invalid-name
    '''Show status of containers'''
    containers = get_containers(up)

    display_containers(containers)


def get_containers(up):  # pylint: disable=invalid-name
    '''Get all containers'''
    client = docker.from_env()

    if up:
        return client.containers.list()

    return client.containers.list(all=True)


def display_containers(containers):
    '''Display container status'''
    table = Table()
    table.add_column('Id', style='blue', no_wrap=True)
    table.add_column('Name', no_wrap=True)
    table.add_column('Status', no_wrap=True)

    for container in containers:
        table.add_row(
            container.short_id, container.name,
            format_status(container.status)
        )

    console = Console()
    console.print(table)


def format_status(container_status):
    '''Format container status'''
    if container_status == 'running':
        return f"[green]{fa.icons['arrow-up']}[/green]"

    return f"[red]{fa.icons['arrow-down']}[/red]"
