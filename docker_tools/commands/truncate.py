'''Truncate container logs'''
import logging
import subprocess
import docker
import click
import inquirer

logger = logging.getLogger(__name__)


@click.command()
def truncate():
    '''Truncate container logs'''
    containers = get_container_list()

    selection = select_containers(containers)

    truncate_logs(selection)


def get_container_list():
    '''Get a list of all containers'''
    client = docker.from_env()

    return client.containers.list()


def select_containers(containers):
    '''Select containers of which to truncate the logs'''
    choices = {}
    for container in containers:
        choices[container.attrs['Name']] = container

    questions = [
        inquirer.Checkbox(
            'containers',
            message="Select containers",
            choices=choices
        ),
    ]
    names = inquirer.prompt(questions)

    return [x for x in containers if x.attrs['Name'] in names['containers']]


def truncate_logs(containers):
    '''Truncate logs of list of containers'''
    try:
        for container in containers:
            cmd = f"sudo truncate -s0 {container.attrs['LogPath']}"
            subprocess.run(cmd, shell=True, check=True)
    except subprocess.CalledProcessError as exception:
        logger.error(exception.stderr)
