'''Get IP addresses of containers'''
import logging
import docker
import click
import inquirer
from rich.console import Console
from rich.table import Table
import rich

logger = logging.getLogger(__name__)

BUILTIN_NETWORKS = [
    'bridge',
    'docker_gwbridge',
    'host',
    'ingress',
    'none',
]


@click.command()
def ips():
    '''Get a list of container IPs'''
    networks = get_network_list()

    network = select_network(networks)

    display_ips(network)


def get_network_list():
    '''Get a list of all non-standard docker networks'''
    client = docker.from_env()
    networks = client.networks.list(greedy=True)

    return [x for x in networks if x.attrs['Name'] not in BUILTIN_NETWORKS]


def select_network(networks):
    '''Select a network'''
    choices = []
    for network in networks:
        choices.append(network.attrs['Name'])

    questions = [
        inquirer.List('network', 'Choose a network', choices=choices)
    ]

    selection = inquirer.prompt(questions)

    return [x for x in networks if x.attrs['Name'] == selection['network']][0]


def display_ips(network):
    '''Display a network\'s IPs'''
    table = Table()
    table.add_column('Name', style='blue', no_wrap=True)
    table.add_column('IPv4', no_wrap=True)

    rich.inspect(network.attrs['Containers'].items())

    for container in network.attrs['Containers'].values():
        table.add_row(container['Name'], container['IPv4Address'])

    console = Console()
    console.print(table)
