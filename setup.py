'''Setup'''
from setuptools import setup, find_packages


def read_requirements():
    '''Read requirements from file'''
    with open('requirements.txt', 'r') as reqs:
        contents = reqs.read()
        requirements = contents.split('\n')

    return requirements


with open('LICENSE') as file:
    license_terms = file.read()


setup(name='docker-tools',
      version='0.1',
      author='Roman Handke',
      author_email='roman.handke@online.de',
      description='A collection of scripts simplifying tasks around docker',
      packages=find_packages(),
      install_requires=read_requirements(),
      license=license_terms,
      entry_points={'console_scripts': ['dt=docker_tools.main:cli']})
